class DataExtraction:
    """
    Class to extract the data
    """
    def data_extraction(self,filename):
        self.data = sorted(open(filename,'r').readlines())
        return self.data